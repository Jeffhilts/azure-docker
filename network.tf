 resource "azurerm_network_security_group" "secgrp" {
   name                = "${var.hostname}-secgrp"
   location            = "${azurerm_resource_group.rg.location}"
   resource_group_name = "${azurerm_resource_group.rg.name}"
 }

resource "azurerm_network_security_rule" "ssh_access" {
    name = "${var.hostname}-ssh-access-rule"
    priority                    = 200
    direction                   = "Inbound"
    access                      = "Allow"
    protocol                    = "Tcp"
    source_port_range           = "*"
    destination_port_range      = "22"
    source_address_prefix       = "*"
    destination_address_prefix  = "*"
    resource_group_name         = "${azurerm_resource_group.rg.name}"
    network_security_group_name = "${azurerm_network_security_group.secgrp.name}"
  }

 resource "azurerm_virtual_network" "vnet" {
   name                 = "${var.rg_prefix}-vnet"
   name                = "${var.virtual_network_name}"
   location            = "${var.location}"
   address_space       = ["${var.address_space}"]
   resource_group_name = "${azurerm_resource_group.rg.name}"
 }

 resource "azurerm_subnet" "subnet" {
   name                 = "${var.rg_prefix}-subnet"
   virtual_network_name = "${azurerm_virtual_network.vnet.name}"
   resource_group_name  = "${azurerm_resource_group.rg.name}"
   address_prefix       = "${var.subnet_prefix}"
 }

 resource "azurerm_public_ip" "pip" {
   name                         = "${var.rg_prefix}-ip"
   location                     = "${var.location}"
   resource_group_name          = "${azurerm_resource_group.rg.name}"
   allocation_method = "Dynamic"
   domain_name_label            = "${var.dns_name}"
 }

 resource "azurerm_network_interface" "nic" {
   name                = "${var.rg_prefix}nic"
   location            = "${var.location}"
   resource_group_name = "${azurerm_resource_group.rg.name}"
   network_security_group_id = "${azurerm_network_security_group.secgrp.id}"
   ip_configuration {
     name                          = "${var.rg_prefix}ipconfig"
     subnet_id                     = "${azurerm_subnet.subnet.id}"
     private_ip_address_allocation = "Dynamic"
     public_ip_address_id          = "${azurerm_public_ip.pip.id}"
  }
 }
