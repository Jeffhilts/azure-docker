data "azurerm_resource_group" "rg" {
  name     = "${var.resource_group}"
}

data "azurerm_network_security_group" "secgrp" {
  name                = "NSG-QA-IHS"
  resource_group_name = "QANCCI-RSG-NetSecGrp"
}

data "azurerm_virtual_network" "vnet" {
   name ="QANCCI-VNet"
   resource_group_name = "QANCCI-RSG-VNet"
 }

data "azurerm_subnet" "subnet" {
   name                 = "10_18_100_0_CloudResearch"
   virtual_network_name = "QANCCI-VNet"
   resource_group_name  = "QANCCI-RSG-VNet"
 }

resource "azurerm_public_ip" "pip" {
  name                         = "${var.rg_prefix}-ncciip"
  location                     = "${var.location}"
  resource_group_name          = "${data.azurerm_resource_group.rg.name}"
  allocation_method = "Dynamic"
  domain_name_label            = "${var.dns_name}-ncci"
}

resource "azurerm_network_interface" "nccinic" {
  name                = "${var.rg_prefix}-nccinic"
  location            = "${var.location}"
  resource_group_name = "${data.azurerm_resource_group.rg.name}"
  network_security_group_id = "${data.azurerm_network_security_group.secgrp.id}"
  ip_configuration {
    name                          = "${var.rg_prefix}ipconfig"
    subnet_id                     = "${data.azurerm_subnet.subnet.id}"
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = "${azurerm_public_ip.pip.id}"
 }
}