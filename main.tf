
resource "azurerm_resource_group" "rg" {
  name     = "${var.resource_group}"
  location = "${var.location}"
}

resource "azurerm_storage_account" "stor" {
  name                     = "${var.dns_name}stor"
  location                 = "${var.location}"
  resource_group_name      = "${azurerm_resource_group.rg.name}"
  account_tier             = "${var.storage_account_tier}"
  account_replication_type = "${var.storage_replication_type}"
}

resource "azurerm_managed_disk" "datadisk" {
  name                 = "${var.hostname}-datadisk"
  location             = "${var.location}"
  resource_group_name  = "${azurerm_resource_group.rg.name}"
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "vm" {
  name                  = "${var.rg_prefix}vm"
  location              = "${var.location}"
  resource_group_name   = "${azurerm_resource_group.rg.name}"
  vm_size               = "${var.vm_size}"
  network_interface_ids = ["${azurerm_network_interface.nic.id}"]
  
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "${var.image_publisher}"
    offer     = "${var.image_offer}"
    sku       = "${var.image_sku}"
    version   = "${var.image_version}"
  }

  storage_os_disk {
    name              = "${var.hostname}-osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

    storage_data_disk {
    name              = "${var.hostname}-datadisk"
    managed_disk_id   = "${azurerm_managed_disk.datadisk.id}"
    managed_disk_type = "Standard_LRS"
    disk_size_gb      = "1023"
    create_option     = "Attach"
    lun               = 0
  }

  os_profile {
    computer_name  = "${var.hostname}"
    admin_username = "${var.vm_username}"
    admin_password = "${var.vm_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
#    ssh_keys = [{
#      path     = "/home/${var.ssh_user_username}/.ssh/authorized_keys"
#      key_data = "${file("~/.ssh/id_rsa.pub")}"
#    }]
  }

  boot_diagnostics {
    enabled     = true
    storage_uri = "${azurerm_storage_account.stor.primary_blob_endpoint}"
  }


  provisioner "remote-exec" {
    #script = "configure.sh"
    connection {
        type        = "ssh"
        #host        = "${azurerm_network_interface.nic.private_ip_address}"
        host        = "${var.dns_name}.${var.location}.cloudapp.azure.com"
        user        = "${var.vm_username}"
        password    = "${var.vm_password}"
    }

    inline=["echo Update the machine",
    "sudo -S <<< ${var.vm_password} yum install -y yum-utils",
    "sudo -S <<< ${var.vm_password} yum install -y device-mapper-persistent-data",
    "sudo -S <<< ${var.vm_password} yum install -y lvm2",
    "sudo -S <<< ${var.vm_password} yum-config-manager --add-repo  https://download.docker.com/linux/centos/docker-ce.repo",
    "sudo -S <<< ${var.vm_password} yum install -y docker-ce docker-ce-cli containerd.io",
    "sudo -S <<< ${var.vm_password} usermod -aG docker ${var.vm_username}",
    "sudo -S <<< ${var.vm_password} systemctl start docker",
    "sudo -S <<< ${var.vm_password} echo '[Service]' > /etc/systemd/system/docker.service.d/override.conf",
    "sudo -S <<< ${var.vm_password} echo 'ExecStart=' >> /etc/systemd/system/docker.service.d/override.conf",
    "sudo -S <<< ${var.vm_password} echo 'ExecStart=/usr/bin/dockerd --graph=/home/vmadmin/docker --storage-driver=overlay2' >> /etc/systemd/system/docker.service.d/override.conf",
    "sudo -S <<< ${var.vm_password} systemctl daemon-reload",
    "sudo -S <<< ${var.vm_password} systemctl restart docker.service",
    "sudo -S <<< ${var.vm_password} docker pull registry.gitlab.com/yohayster/cvs-lookup-service",
    "sudo -S <<< ${var.vm_password} docker run -d --name cvs-lookup-service -e IDB_UID='zsdidb' -e IDB_PW='whowhat' -e CONNECT_STRING='dbdidb:1521/idb.oracle.ncci.com' -p 8093:8093 registry.gitlab.com/yohayster/cvs-lookup-service"
    ]
  }
}
